/**
Использование текстуры в качестве коэффициента отражения света
*/

#version 330

uniform sampler2D sandTex;
uniform sampler2D grassTex;
uniform sampler2D stoneTex;
uniform sampler2D snowTex;
uniform sampler2D waterTex;
uniform sampler2D mapTex;

in vec4 posCamSpace; // Координаты вершины в системе координат камеры

in vec2 texCoord; //текстурные координаты (интерполирована между вершинами треугольника)
in vec2 mapCoord;
in float h;
in vec3 color_tmp;

in vec4 lightDirCamSpace;
in float NdotL;
in vec3 normalCamSpace;
in vec3 ls;

out vec4 fragColor; //выходной цвет фрагмента

const float sigma = 0.7;

float norm_coef(float x, float mu, float sigma_) {
    return exp(-((x - mu) * (x - mu) / (2 * sigma_*sigma_)));
}

const float shiness = 128.0; // яркость бликов

void main()
{
    vec4 mapColor = texture(mapTex, mapCoord).rgba;

    vec3 diffuseColor = texture(waterTex, texCoord).rgb * norm_coef(h, 0, sigma/70) +
    texture(sandTex, texCoord).rgb * norm_coef(h, 1.5*sigma/7, sigma/7) +
    norm_coef(mapColor.r, sigma, sigma) * texture(grassTex, texCoord).rgb * norm_coef(h, 2.5*sigma/7, sigma/7) +
    norm_coef(mapColor.g, sigma, sigma) * mapColor.g * texture(stoneTex, texCoord).rgb * norm_coef(h, 5*sigma/7, sigma/7) +
    texture(snowTex, texCoord).rgb * norm_coef(h, sigma, 1.5*sigma/7);

    vec3 color = diffuseColor * color_tmp;

    if (NdotL > 0.0)
    {
        vec3 viewDirection = normalize(-posCamSpace.xyz);// Направление на виртуальную камеру
        vec3 halfVector = normalize(lightDirCamSpace.xyz + viewDirection);// Биссектриса между направлением на камеру и на источник света

        float blinnTerm = max(dot(normalCamSpace, halfVector), 0.0);// Интесивность бликового освещения по Блинну
        blinnTerm = pow(blinnTerm, shiness);// Регулируем размер блика

        float Ks = mapColor.a * 10 * norm_coef(h, sigma, sigma/7);
        color += ls * vec3(Ks, Ks, Ks) * blinnTerm;// Добавляем бликовую часть
    }

    fragColor = vec4(color, 1.0);
}