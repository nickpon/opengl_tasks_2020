#version 330

struct LightInfo
{
    vec3 dir; //направление на источник света в мировой системе координат (для направленного источника)
    vec3 La; //цвет и интенсивность окружающего света
    vec3 Ld; //цвет и интенсивность диффузного света
    vec3 Ls; //цвет и интенсивность бликового света
};
uniform LightInfo light;


//стандартные матрицы для преобразования координат
uniform mat4 modelMatrix; //из локальной в мировую
uniform mat4 viewMatrix; //из мировой в систему координат камеры
uniform mat4 projectionMatrix; //из системы координат камеры в усеченные координаты

//матрица для преобразования нормалей из локальной системы координат в систему координат камеры
uniform mat3 normalToCameraMatrix;

layout(location = 0) in vec3 vertexPosition; //координаты вершины в локальной системе координат
layout(location = 1) in vec3 vertexNormal; //нормаль в локальной системе координат
layout(location = 2) in vec2 vertexTexCoord; //текстурные координаты вершины
layout(location = 3) in vec2 vertexMapCoord; //текстурные координаты для карты рельефа

out vec2 texCoord; //текстурные координаты
out vec2 mapCoord;
out float h;
out vec3 color_tmp;

out vec4 lightDirCamSpace;
out float NdotL;
out vec3 normalCamSpace;
out vec3 ls;
out vec4 posCamSpace; // Координаты вершины в системе координат камеры

void main()
{
    gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(vertexPosition, 1.0);
    posCamSpace = viewMatrix * modelMatrix * vec4(vertexPosition, 1.0);
    texCoord = vertexTexCoord;
    mapCoord = vertexMapCoord;

    normalCamSpace = normalize(normalToCameraMatrix * vertexNormal); //преобразование нормали в систему координат камеры
    lightDirCamSpace = viewMatrix * normalize(vec4(light.dir, 0.0));
    NdotL = max(dot(normalCamSpace, lightDirCamSpace.xyz), 0.0); //скалярное произведение (косинус)

    color_tmp = (light.La + light.Ld * NdotL);

    h = vertexPosition[2];
    ls = light.Ls;
}