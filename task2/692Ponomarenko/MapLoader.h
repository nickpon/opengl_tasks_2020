#pragma once

#ifndef OPENGL_TASKS_2020_MAPLOADER_H
#define OPENGL_TASKS_2020_MAPLOADER_H

#include <iostream>
#include <vector>

class MapLoader {
public:
    int w;
    int h;
    float maxHight;
    const char* pathToPNG;

    std::vector<std::vector<float>> htMap;

    MapLoader();
    void ReadMap();

private:
    unsigned char* LoadSOIL();
};


#endif //OPENGL_TASKS_2020_MAPLOADER_H
