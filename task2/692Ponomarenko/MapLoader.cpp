#include "MapLoader.h"
#include "../../external/SOIL/src/SOIL2/SOIL2.h"
#include <iostream>

MapLoader::MapLoader()
{
    maxHight = 0;
    w = 0;
    h = 0;
    pathToPNG = "692PonomarenkoData2/iceland_1200.png";
}

void MapLoader::ReadMap() {
    unsigned char* map = LoadSOIL();

    for (int i = 0; i < h; ++i) {
        htMap.emplace_back(w);
        for (int j = 0; j < w; ++j) {
            htMap[i][j] = (float)(map[j]);
        }
        maxHight = std::max(
                maxHight,
                *std::max_element(std::begin(htMap[i]), std::end(htMap[i]))
                );
        map += int(w); // next row
    }
    // Normalize to 0-1.
    for (int i = 0; i < h; ++i) {
        for (int j = 0; j < w; ++j) {
            htMap[i][j] /= maxHight;
        }
    }
}

unsigned char* MapLoader::LoadSOIL() {
    int channels = 1;
    unsigned char *map = SOIL_load_image(pathToPNG, &w, &h, &channels, SOIL_LOAD_L);
    if ((channels != 1) || (map == nullptr)) {
        std::__throw_runtime_error("error");
    }
    return map;
}