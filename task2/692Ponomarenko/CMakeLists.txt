add_compile_definitions(GLM_ENABLE_EXPERIMENTAL)

set(SRC_FILES
	common/Application.cpp
	common/DebugOutput.cpp
	common/Camera.cpp
	common/Mesh.cpp
	common/ShaderProgram.cpp
	common/Texture.cpp
	main.cpp
)

set(HEADER_FILES
	common/Application.hpp
	common/DebugOutput.h
	common/Camera.hpp
	common/Mesh.hpp
	common/Texture.hpp
	common/ShaderProgram.hpp
	common/LightInfo.hpp
	MapLoader.h
	PlayerCamera.h
)

include_directories(common)

MAKE_OPENGL_TASK(692Ponomarenko 2 "${SRC_FILES}")