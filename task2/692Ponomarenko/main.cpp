#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>
#include <Texture.hpp>
#include <LightInfo.hpp>

#include <vector>
#include <glm/gtx/transform.hpp>
#include <algorithm>
#include "MapLoader.h"
#include "MapLoader.cpp"

#include "PlayerCamera.h"


class SampleApplication : public Application
{
public:
    MapLoader map;
    MeshPtr _surface;
    ShaderProgramPtr _shader;
    std::vector<glm::vec3> _pnts;
    std::vector<glm::vec3> _norms;
    std::vector<glm::vec2> _texts;

    LightInfo _light;
    float _phi = 1.42;
    float _theta = glm::pi<float>() * 0.2f;

    TexturePtr _snow_texture;
    TexturePtr _sand_texture;
    TexturePtr _stone_texture;
    TexturePtr _grass_texture;

    TexturePtr _water_texture;
    TexturePtr _map_texture;

    GLuint _sampler;

    float scale = 20;
    const char* shaderVert = "692PonomarenkoData2/shader.vert";
    const char* shaderFrag = "692PonomarenkoData2/shader.frag";

    const char* snowTexture = "692PonomarenkoData2/snow.jpg";
    const char* sandTexture = "692PonomarenkoData2/sand.jpg";
    const char* stoneTexture = "692PonomarenkoData2/stone.jpg";
    const char* grassTexture = "692PonomarenkoData2/grass.jpg";
    const char* waterTexture = "692PonomarenkoData2/water.png";
    const char* maskTexture = "692PonomarenkoData2/iceland_1200_map.jpg"; // Mask for the relief.
    // In Application constructor we use FreeCameraMove.
    // That is why we don't need to think about camera anymore.

    SampleApplication() {
        map.ReadMap();
        countPointAndNorms();
    }

    void makeScene() override
    {
        float scale = 20;

        Application::makeScene();

        _cameraMover = std::make_shared<PlayerCamera>(map, scale);

        _surface = generateMesh();

        glm::tmat4x4<double> translationMatrix = glm::translate(glm::mat4(1.0f), glm::vec3(-scale / 2, -scale / 2, 0.0f));
        glm::vec3 scalingVector = glm::vec3(1.f * scale, 1.f * scale, 0.5);
        glm::tmat4x4<double> scalingMatrix = glm::scale(scalingVector);
        glm::tmat4x4<double> modelMatrix = translationMatrix * scalingMatrix;

        _surface->setModelMatrix(modelMatrix);
        _shader = std::make_shared<ShaderProgram>(shaderVert, shaderFrag);

        _snow_texture = loadTexture(snowTexture);
        _sand_texture = loadTexture(sandTexture);
        _stone_texture = loadTexture(stoneTexture);
        _grass_texture = loadTexture(grassTexture);
        _water_texture = loadTexture(waterTexture);
        _map_texture = loadTexture(maskTexture);

        _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta));
        _light.ambient = glm::vec3(0.2, 0.2, 0.2);
        _light.diffuse = glm::vec3(0.8, 0.8, 0.8);
        _light.specular = glm::vec3(1.0, 1.0, 1.0);

        glGenSamplers(1, &_sampler);
        glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glSamplerParameterf(_sampler, GL_TEXTURE_MAX_ANISOTROPY_EXT, static_cast<GLfloat>(4));
    }

    void draw() override
    {
        Application::draw();
        int w, h;
        glfwGetFramebufferSize(_window, &w, &h);
        glViewport(0, 0, w, h);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // NOLINT(hicpp-signed-bitwise)

        _shader->use();
        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        glm::vec3 lightDir = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta));

        _shader->setVec3Uniform("light.dir", lightDir);
        _shader->setVec3Uniform("light.La", _light.ambient);
        _shader->setVec3Uniform("light.Ld", _light.diffuse);
        _shader->setVec3Uniform("light.Ls", _light.specular);

        GLuint textureUnitSand = 0;
        GLuint textureUnitGrass = 1;
        GLuint textureUnitStone = 2;
        GLuint textureUnitSnow = 3;

        glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0
        glBindSampler(0, _sampler);
        _snow_texture->bind();
        _shader->setIntUniform("snowTex", 0);

        glActiveTexture(GL_TEXTURE0 + 1);
        _grass_texture->bind();
        _shader->setIntUniform("grassTex", 1);

        glActiveTexture(GL_TEXTURE0 + 2);
        _sand_texture->bind();
        _shader->setIntUniform("sandTex", 2);

        glActiveTexture(GL_TEXTURE0 + 3);
        _water_texture->bind();
        _shader->setIntUniform("waterTex", 3);

        glActiveTexture(GL_TEXTURE0 + 4);
        _map_texture->bind();
        _shader->setIntUniform("mapTex", 4);

        glActiveTexture(GL_TEXTURE0 + 5);
        _stone_texture->bind();
        _shader->setIntUniform("stoneTex", 5);

        {
            _shader->setMat4Uniform("modelMatrix", _surface->modelMatrix());
            _shader->setMat3Uniform("normalToCameraMatrix",
                                    glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _surface->modelMatrix()))));
            _surface->draw();
        }

        glBindSampler(0, 0);
        glUseProgram(0);
    }

    void updateGUI() override
    {
        Application::updateGUI();

        ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
        if (ImGui::Begin("MIPT OpenGL Sample", NULL, ImGuiWindowFlags_AlwaysAutoResize))
        {
            ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

            if (ImGui::CollapsingHeader("Light"))
            {
                ImGui::SliderFloat("phi", &_phi, 0.0f, 2.0f * glm::pi<float>());
                ImGui::SliderFloat("theta", &_theta, 0.0f, glm::pi<float>());
            }
        }
        ImGui::End();
    }

private:
    void countPointAndNorms() {
        for (int i = 0; i < map.h - 1; ++i) {
            for (int j = 0; j < map.w - 1; ++j) {
                for (auto & pnt : std::vector<std::vector<int>>(
                        {{0, 0}, {0, 1}, {1, 0}, {0, 1}, {1, 0}, {1, 1}}
                )) {
                    int i_ = i + pnt[0];
                    int j_ = j + pnt[1];

                    assert(map.htMap[i_][j_] <= 1.0);

                    _pnts.emplace_back(float(i_)/(map.h - 1), float(j_)/(map.w - 1),
                                                map.htMap[i_][j_]);
                    _norms.push_back(get_normal(map.htMap, map.h, map.w, i_, j_));
                    _texts.emplace_back(float(i_)/20, float(j_)/20);
                }
            }
        }
    }

    glm::vec3 get_normal(const std::vector<std::vector<float>>& map, int height, int width, int i, int j) {
        int left = j, right = j, bottom = i, top = i;
        if (i > 0) {
            top = i - 1;
        }
        if (i < height - 1) {
            bottom = i + 1;
        }
        if (j > 0) {
            left = j - 1;
        }
        if (j < width - 1) {
            right = j + 1;
        }
        glm::vec3 horizonal = glm::vec3(2, map[i][right] - map[i][left], 0);
        glm::vec3 vertical = glm::vec3(0, map[top][j] - map[bottom][j], 2);
        glm::vec3 normal = glm::normalize(glm::cross(vertical, horizonal));

        return normal;
    }

    DataBufferPtr getPoints() {
        DataBufferPtr buf_points = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf_points->setData(_pnts.size() * sizeof(float) * 3, _pnts.data());
        return buf_points;
    }

    DataBufferPtr getNorms() {
        DataBufferPtr buf_norms = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf_norms->setData(_norms.size() * sizeof(float) * 3, _norms.data());
        return buf_norms;
    }

    DataBufferPtr getTexts() {
        DataBufferPtr buf_textures = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf_textures->setData(_texts.size() * sizeof(float) * 2, _texts.data());
        return buf_textures;
    }

    MeshPtr generateMesh() {
        DataBufferPtr buf_points = getPoints();
        DataBufferPtr buf_norms = getNorms();
        DataBufferPtr buf_textures = getTexts();

        auto surface = std::make_shared<Mesh>();
        surface->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf_points);
        surface->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf_norms);
        surface->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf_textures);

        surface->setPrimitiveType(GL_TRIANGLES);
        surface->setVertexCount(_pnts.size());

        surface->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.5f)));
        return surface;
    }

    glm::tmat4x4<double> getModelMatrix() {
        glm::tmat4x4<double> translationMatrix = glm::translate(glm::mat4(1.0f), glm::vec3(-scale / 2, -scale / 2, 0.0f));
        glm::vec3 scalingVector = glm::vec3(1.f * scale, 1.f * scale, 0.5);
        glm::tmat4x4<double> scalingMatrix = glm::scale(scalingVector);
        return translationMatrix * scalingMatrix;
    }
};

int main()
{
    SampleApplication app;
    app.start();
    return 0;
}