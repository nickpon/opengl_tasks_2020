#pragma once

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/quaternion.hpp>

#include "common/Camera.hpp"
#include "MapLoader.h"

#ifndef OPENGL_TASKS2020_PLAYERCAMERA_H
#define OPENGL_TASKS2020_PLAYERCAMERA_H


class PlayerCamera: public FreeCameraMover {
public:
    PlayerCamera(MapLoader& height_map_, double scale_):
        FreeCameraMover(),
        height_map(height_map_),
        scale(scale_) {
        _pos = glm::fvec3(0, 0, 0);
        correct_height();
        _rot = glm::toQuat(glm::lookAt(_pos, glm::vec3(1.0f, 0.0f, 0.f), glm::vec3(0.0f, 0.0f, 1.0f)));
    }


    void update(GLFWwindow* window, double dt)
    {
        float speed = 1.0f;

    //   абсолютно вверх
        glm::vec3 upDir = glm::vec3(0.0f, 0.0f, 1.0f);

    //   вправо в мировой
        glm::vec3 rightDir = glm::vec3(1.0f, 0.0f, 0.0f) * _rot;

    //  вперед не в surface, а над surface
        glm::vec3 forwDir = glm::cross(upDir, rightDir);


        if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
            _pos += forwDir * speed * static_cast<float>(dt);
        }
        if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
            _pos -= forwDir * speed * static_cast<float>(dt);
        }
        if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
            _pos -= rightDir * speed * static_cast<float>(dt);
        }
        if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
            _pos += rightDir * speed * static_cast<float>(dt);
        }

        correct_height();

        //-----------------------------------------

        //Соединяем перемещение и поворот вместе
        _camera.viewMatrix = glm::toMat4(-_rot) * glm::translate(-_pos);

        //-----------------------------------------

        int width, height;
        glfwGetFramebufferSize(window, &width, &height);

        //Обновляем матрицу проекции на случай, если размеры окна изменились
        _camera.projMatrix = glm::perspective(glm::radians(45.0f), (float)width / height, _near, _far);
    }

private:
    MapLoader height_map;
    double scale;

    void correct_height() {
        int i = (_pos.x + scale/2)*(height_map.w - 1)/scale;
        int j = (_pos.y + scale/2)*(height_map.h - 1)/scale;
        if (i < height_map.w && j < height_map.h) {
            _pos.z = height_map.htMap[i][j] + 0.05f;
        }
    }
};

#endif //OPENGL_TASKS2020_PLAYERCAMERA_H
