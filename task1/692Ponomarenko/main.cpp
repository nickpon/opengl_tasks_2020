#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>
#include <vector>
#include <glm/gtx/transform.hpp>
#include <algorithm>
#include "MapLoader.h"
#include "MapLoader.cpp"


class SampleApplication : public Application
{
public:
    MapLoader map;
    MeshPtr _surface;
    ShaderProgramPtr _shader;
    std::vector<glm::vec3> _pnts;
    std::vector<glm::vec3> _norms;
    float scale = 20;
    const char* shaderVert = "692PonomarenkoData1/shader.vert";
    const char* shaderFrag = "692PonomarenkoData1/shader.frag";
    // In Application constructor we use FreeCameraMove.
    // That is why we don't need to think about camera anymore.

    SampleApplication() {
        map.ReadMap();
        countPointAndNorms();
    }

    void makeScene() override
    {
        Application::makeScene();
        _surface = generateMesh();
        _surface->setModelMatrix(getModelMatrix());
        _shader = std::make_shared<ShaderProgram>(shaderVert, shaderFrag);
    }

    void draw() override
    {
        Application::draw();
        int w, h;
        glfwGetFramebufferSize(_window, &w, &h);
        glViewport(0, 0, w, h);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // NOLINT(hicpp-signed-bitwise)
        _shader->use();
        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);
        _shader->setMat4Uniform("modelMatrix", _surface->modelMatrix());
        _surface->draw();
    }

private:
    void countPointAndNorms() {
        for (int i = 0; i < map.h - 1; ++i) {
            for (int j = 0; j < map.w - 1; ++j) {
                for (auto & pnt : std::vector<std::vector<int>>(
                        {{0, 0}, {0, 1}, {1, 0}, {0, 1}, {1, 0}, {1, 1}}
                )) {
                    _pnts.emplace_back(
                            float(i + pnt[0])/(float(map.h) - 1.f),
                            float(j + pnt[1])/(float(map.w) - 1.f),
                            map.htMap[i + pnt[0]][j + pnt[1]]
                    );
                    _norms.emplace_back(0, 1, 0);
                }
            }
        }
    }

    DataBufferPtr getPoints() {
        DataBufferPtr buf_points = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf_points->setData(_pnts.size() * sizeof(float) * 3, _pnts.data());
        return buf_points;
    }

    DataBufferPtr getNorms() {
        DataBufferPtr buf_norms = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf_norms->setData(_norms.size() * sizeof(float) * 3, _norms.data());
        return buf_norms;
    }

    MeshPtr generateMesh() {
        DataBufferPtr buf_points = getPoints();
        DataBufferPtr buf_norms = getNorms();
        auto surface = std::make_shared<Mesh>();
        surface->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf_points);
        surface->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf_norms);
        surface->setPrimitiveType(GL_TRIANGLES);
        surface->setVertexCount(_pnts.size());
        surface->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.5f)));
        return surface;
    }

    glm::tmat4x4<double> getModelMatrix() {
        glm::tmat4x4<double> translationMatrix = glm::translate(glm::mat4(1.0f), glm::vec3(-scale / 2, -scale / 2, 0.0f));
        glm::vec3 scalingVector = glm::vec3(1.f * scale, 1.f * scale, 0.5);
        glm::tmat4x4<double> scalingMatrix = glm::scale(scalingVector);
        return translationMatrix * scalingMatrix;
    }
};

int main()
{
    SampleApplication app;
    app.start();
    return 0;
}