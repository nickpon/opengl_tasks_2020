#version 330

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;

layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec3 vertexNormal;

out vec4 color;

void main()
{
    float red_dist = 2 * (0.6 - vertexPosition[2]) * int(vertexPosition[2] < 0.6);
    float green_dist = 2 * vertexPosition[2] * int(vertexPosition[2] <= 0.7) +
    (2 - 2 * vertexPosition[2]) * int(vertexPosition[2] > 0.7);
    float blue_dist = 2 * (vertexPosition[2] - 0.5) * int(vertexPosition[2] > 0.5);
    color.rgb = vec3(blue_dist, green_dist, red_dist);
    color.a = 1;
    gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(vertexPosition, 1.0);
}